#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

enable = 26 # GPIO7


GPIO.setup(enable, GPIO.OUT) 

GPIO.output(enable, GPIO.LOW)
try:
    while True:    
        print('duty')   
        GPIO.output(enable, GPIO.HIGH) 	
	time.sleep(1)  
except KeyboardInterrupt:
    print('keyboard interrupt')

except:
    print('except')

finally: 
    GPIO.cleanup()
    print('finally') 
