#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

speed = 12 # Connected to GPIO 18, PWM0 (pin 12)
#0.15 s per 60 grader

# set up GPIO pins
GPIO.setup(speed, GPIO.OUT) 
pi_pwm = GPIO.PWM(speed, 50) #1000 hz
pi_pwm.start(0)

try:
    while True:     # range between 5-6 is ideal, but between 5-7 also works (not symmetrical)
        print('duty 5') 
	pi_pwm.ChangeDutyCycle(5) #provide duty cycle in the range 0-100       	
	time.sleep(5)  
        print('duty 6') 
        pi_pwm.ChangeDutyCycle(6) 
	time.sleep(5)  
        #print('duty 75')  
        #pi_pwm.ChangeDutyCycle(75) 
	#time.sleep(2) 
        #print('duty 100')  
        #pi_pwm.ChangeDutyCycle(100) 
	#time.sleep(2) 
        #print('duty 50')  
        #pi_pwm.ChangeDutyCycle(50) 
	#time.sleep(2) 
except KeyboardInterrupt:
    pi_pwm.stop()
    GPIO.cleanup()


	           

