#!/usr/bin/env python

# Import required modules
import time
import RPi.GPIO as GPIO

# Declare the GPIO settings
GPIO.setmode(GPIO.BOARD)

speed = 22 # Connected to GPIO 25, (pin 22)
enable = 26 # GPIO7


# set up GPIO pins
GPIO.setup(speed, GPIO.OUT) 
GPIO.setup(enable, GPIO.OUT) 

pi_pwm = GPIO.PWM(speed, 1000) #1000 hz
pi_pwm.start(0)

GPIO.output(enable, GPIO.HIGH)
try:
    while True:  
        duty = 100   
	pi_pwm.ChangeDutyCycle(duty) #provide duty cycle in the range 0-100    
        print('duty', duty)    	
	time.sleep(1)  
except KeyboardInterrupt:
    print('keyboard interrupt')
    pi_pwm.stop()
    GPIO.cleanup()


	           

